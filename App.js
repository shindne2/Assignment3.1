import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	View,
	Image,
	NavBar,
	Button,
	Alert
} from 'react-native';

import {
  StackNavigator,
  TabNavigator,
} from 'react-navigation';


import Followers from './Followers';
import Following from './Following';
import ProfilePage from './ProfilePage';
import RealRepo from './RealRepo';


var MainScreenNavigator = TabNavigator({
	Profile: {screen: ProfilePage},
	Followers: {screen: Followers},
	Following: {screen: Following},
	Repos: {screen: RealRepo}
},{
	tabBarPosition:'bottom',
	tabBarOptions:{
		activeTintColor: 'powderblue',
		activeBackgroundColor: 'white'
	}
	
})

MainScreenNavigator.navigationOptions = {
	title: "Tab Example"
};

export default MainScreenNavigator;