import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	View,
	Image,
	NavBar,
	Button,
	Alert,
	ScrollView,
	Linking
} from 'react-native';

import {
  StackNavigator,
  navigate
} from 'react-navigation';

import { List, ListItem } from 'react-native-elements';



export default class Following extends Component {
_onPress() {
  Alert.alert('on Press!');
 }
nothing(){
}

 state = {
      names: [
         {'name': 'Bob', 'tag': 'bobbbb', 'url' : 'http://www.github.com/'},
         {'name': 'Steve','tag': 'steve123','url' : 'http://www.github.com/'},
         {'name': 'Carrol','tag': 'care-role','url' : 'http://www.github.com/'},
         {'name': 'Matt','tag': 'mattylight','url' : 'http://www.github.com/'},
         {'name': 'Gabe','tag': 'gabegabe','url' : 'http://www.github.com/'},
         {'name': 'Peter','tag': 'retep','url' : 'http://www.github.com/'},
         {'name': 'Austin','tag': 'austinTexas','url' : 'http://www.github.com/'},
         {'name': 'Bobby','tag': 'bobbbbee','url' : 'http://www.github.com/'},
         {'name': 'Alex','tag': '_alex_a','url' : 'http://www.github.com/'},
         {'name': 'Max','tag': 'tothemax','url' : 'http://www.github.com/'},
         {'name': 'Mike','tag': 'mik3','url' : 'http://www.github.com/'},
         {'name': 'Carly','tag': 'car-lee','url' : 'http://www.github.com/'},
         {'name': 'Sam','tag': 'samsam','url' : 'http://www.github.com/'},
      ]
   }

render() {
    return (
    	<View style={{flex: 1}}>
				<View style ={{flex: 2, backgroundColor: 'powderblue',  flexDirection: 'column',
	    justifyContent: 'center',
	    alignItems: 'center'}}>
					<Text style ={{color:'white', fontSize: 30}}>
					Following
					</Text>
				</View>
				<View style = {{flex:5, backgroundColor:'white'}}>
					  <ScrollView>
						{this.state.names.map((item, index) => (
						            <ListItem
						              key={item.name}
						              title={item.name.toUpperCase()}
						              subtitle = {item.tag}
						              onPress={()=>{ Linking.openURL(item.url)}}/>
						 ))}
  					  </ScrollView>
				</View>
				
			</View>
    );
  }
}




const styles = StyleSheet.create({
	navigationBar: {
		borderTopWidth: 10,
		flex: 0,
		backgroundColor: '#444',
		flexDirection: 'row',
		height: 50
	},
	navBarSep: {
		borderRightWidth: 2,
		borderLeftWidth: 2,
		flex: 1,
		borderRightColor: 'black',
		borderLeftColor: 'black'
	},
	bar: {
		borderTopColor: 'black',
		borderTopWidth: 4,
		backgroundColor: 'powderblue',
		flexDirection: 'row',
		borderBottomColor: 'black',
		borderBottomWidth: 4
	},
	barseparator: {
		borderRightWidth: 4,
		borderLeftWidth: 4,
		flex: 1,
		borderRightColor: 'black',
		borderLeftColor: 'black'
	},
	barTop: {
		color: '#fff',
		fontSize: 16,
		fontWeight: 'bold',
		fontStyle: 'italic'
	},
	barBottom: {
		color: '#fff',
		fontSize: 14,
		fontWeight: 'bold'
	}
});

// skip this line if using Create React Native App









