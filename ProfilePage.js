import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	View,
	Image,
	NavBar,
	Button,
	Alert
} from 'react-native';

import {
  StackNavigator,
  navigate
} from 'react-navigation';


export default class ProfilePage extends Component {
_onPress() {
  Alert.alert('on Press!');
 }
nothing(){
}

render() {
    return (
      // Try removing the `flex: 1` on the parent View.
      // The parent will not have dimensions, so the children can't expand.
      // What if you add `height: 300` instead of `flex: 1`?
      <View style={{flex: 1}}>
	    <View style={{flex: 3, backgroundColor: '#C0C0C0',  flexDirection: 'column',
	    justifyContent: 'center',
	    alignItems: 'center',}}>
	   		<Image source={require('./avatar.png')} style = {{width:100, height: 100}} />
	    <Text> Gabriel Shindnes </Text>
	    <Text> @gshind </Text>
	    <Text style = {{fontSize: 10}}> Created 08/15/2013 </Text>
	    </View>
	    <View style = {styles.bar}>
        	<View style ={[styles.barItem, styles.barseparator]} >
        		<Text style = {styles.barTop} >10</Text>
        		<Text style = {styles.barBottom}>Public Repos</Text>
        	</View>
        	<View style ={styles.barseparator} >
        		<Text style = {styles.barTop} >13</Text>
        		<Text style = {styles.barBottom}>Following</Text>
        	</View>
        	<View style ={styles.barseparator} >
        		<Text style = {styles.barTop} >25k</Text>
        		<Text style = {styles.barBottom}>Followers</Text>
        	</View>
        </View>
        <View style={{flex: 3, backgroundColor: 'powderblue'}}>
        	<View style = {{flex: 2, flexDirection: 'row',alignItems: 'center',justifyContent: 'center' }}>
        	<Text style = {{fontSize: 20}}>
        		<Text style = {{fontWeight: 'bold'}}>Bio:</Text>
        		I am new at this 
        	</Text>
        	</View>
        	<View style = {{flex: 2, flexDirection: 'row',alignItems: 'center',justifyContent: 'center' }}>
        	<Text style = {{fontSize: 20}}>
        		<Text style = {{fontWeight: 'bold'}}>Website: </Text>
        		www.gabeshindnes.com
        	</Text>
        	</View>
        	<View style = {{flex: 2, flexDirection: 'row',alignItems: 'center',justifyContent: 'center' }}>
        	<Text style = {{fontSize: 20}}>
        		<Text style = {{fontWeight: 'bold'}}>Email: </Text>
        		gabshind@gmail.com
        	</Text>
        	</View>
        </View>
      </View>
      
    );
  }
}




const styles = StyleSheet.create({
	navigationBar: {
		borderTopWidth: 10,
		flex: 0,
		backgroundColor: '#444',
		flexDirection: 'row',
		height: 50
	},
	navBarSep: {
		borderRightWidth: 2,
		borderLeftWidth: 2,
		flex: 1,
		borderRightColor: 'black',
		borderLeftColor: 'black'
	},
	bar: {
		borderTopColor: 'black',
		borderTopWidth: 4,
		backgroundColor: 'blue',
		flexDirection: 'row',
		borderBottomColor: 'black',
		borderBottomWidth: 4
	},
	barseparator: {
		borderRightWidth: 4,
		borderLeftWidth: 4,
		flex: 1,
		borderRightColor: 'black',
		borderLeftColor: 'black'
	},
	barTop: {
		color: '#fff',
		fontSize: 16,
		fontWeight: 'bold',
		fontStyle: 'italic'
	},
	barBottom: {
		color: '#fff',
		fontSize: 14,
		fontWeight: 'bold'
	}
});

// skip this line if using Create React Native App









