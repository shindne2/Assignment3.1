import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	View,
	Image,
	NavBar,
	Button,
	Alert,
	ScrollView,
	Linking
} from 'react-native';

import {
  StackNavigator,
  navigate
} from 'react-navigation';

import { List, ListItem } from 'react-native-elements';



export default class RepoPage extends Component {
_onPress() {
  Alert.alert('on Press!');
 }
 state = {
      names: [
         {'repo_name': 'Traffic', 'owner': 'gshind', 'desc': 'Traffic Modelling', 'url' : 'http://www.github.com'},
         {'repo_name': 'PythonProject', 'owner': 'gshind', 'desc': 'My PythonProject', 'url' : 'http://www.github.com'},
         {'repo_name': 'cs242project', 'owner': 'bobby', 'desc': 'some project I had to do in CS242','url' : 'http://www.github.com'},
         {'repo_name': 'MachineLearning', 'owner': 'bobby', 'desc': 'Machine Learning is Great', 'url' : 'http://www.github.com'},
         {'repo_name': 'SecurityProject', 'owner': 'codingIsCool', 'desc': 'Cryptography', 'url' : 'http://www.github.com'},
         {'repo_name': 'MyProject', 'owner': 'codingIsCool', 'desc': 'My secret project that Ive been working on', 'url' : 'http://www.github.com'},
         {'repo_name': 'PersonalProject', 'owner': 'codingIsCool', 'desc': 'Another personal project', 'url' : 'http://www.github.com'},
         {'repo_name': 'WhoKnows', 'owner': 'lateNightCoding', 'desc': 'Random project', 'url' : 'http://www.github.com'},
         {'repo_name': 'AnotherProject', 'owner': 'lateNightCoding', 'desc': 'Another random project', 'url' : 'http://www.github.com'},
      ]
   }
render() {
    return (
    		<View style={{flex: 1}}>
				<View style ={{flex: 1, backgroundColor: 'powderblue',  flexDirection: 'column',
	    justifyContent: 'center',
	    alignItems: 'center'}}>
					<Text style ={{color:'white', fontSize: 30}}>
					Repositories
					</Text>
				</View>
				<View style = {{flex:5, backgroundColor:'white'}}>
					  <ScrollView>
						{this.state.names.map((item, index) => (
						            <ListItem
						              key={item.repo_name}
						              title={`${item.owner.toUpperCase()}: ${item.repo_name}`}
						              subtitle={item.desc}
						              onPress={()=>{ Linking.openURL(item.url)}}
						              
						              />
						 ))}
  					  </ScrollView>
				</View>
			</View>
    );
  }
}




const styles = StyleSheet.create({
	navigationBar: {
		borderTopWidth: 10,
		flex: 0,
		backgroundColor: '#444',
		flexDirection: 'row',
		height: 50
	},
	navBarSep: {
		borderRightWidth: 2,
		borderLeftWidth: 2,
		flex: 1,
		borderRightColor: 'black',
		borderLeftColor: 'black'
	},
	bar: {
		borderTopColor: 'black',
		borderTopWidth: 4,
		backgroundColor: 'powderblue',
		flexDirection: 'row',
		borderBottomColor: 'black',
		borderBottomWidth: 4
	},
	barseparator: {
		borderRightWidth: 4,
		borderLeftWidth: 4,
		flex: 1,
		borderRightColor: 'black',
		borderLeftColor: 'black'
	},
	barTop: {
		color: '#fff',
		fontSize: 16,
		fontWeight: 'bold',
		fontStyle: 'italic'
	},
	barBottom: {
		color: '#fff',
		fontSize: 14,
		fontWeight: 'bold'
	},
	item: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 30,
      margin: 2,
      borderColor: '#2a4944',
      borderWidth: 1,
      backgroundColor: '#d2f7f1'
   }
});

// skip this line if using Create React Native App









